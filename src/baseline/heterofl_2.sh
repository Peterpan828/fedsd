#!/bin/bash

python split_mix.py --dataset='cifar100' --randomBranch=1 --kd=0;
python split_mix.py --dataset='cifar100' --randomBranch=1 --kd=1;
   
# python heteroFL.py --dataset='cifar100' --kd=0; 
# python heteroFL.py --dataset='cifar100' --base=0 --kd=0;
# python heteroFL.py --dataset='cifar100' --base=1 --kd=0;
# python heteroFL.py --dataset='cifar100' --base=2 --kd=0;
# python heteroFL.py --dataset='cifar100' --base=3 --kd=0;

# python heteroFL.py --dataset='tiny-imagenet' --kd=0; 
# python heteroFL.py --dataset='tiny-imagenet' --base=0 --kd=0;
# python heteroFL.py --dataset='tiny-imagenet' --base=1 --kd=0;
# python heteroFL.py --dataset='tiny-imagenet' --base=2 --kd=0;
# python heteroFL.py --dataset='tiny-imagenet' --base=3 --kd=0;
