# Implementation of SplitMix publised in ICLR22

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import time
import pickle
import copy
import sys
import os
import torch.multiprocessing as mp
import queue
from torch.utils.data import DataLoader, Subset

sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

import arguments
from dataLoader.dataLoaders import getNodeIndicies
from dataLoader.dataset import get_dataset
from nn_models import resnet_sbn, resnet_sbn_tiny, vggnet_sbn

def select_level(id, args):
    if args.randomBranch == 0:
        i = id / args.nodes
    else:
        i = np.random.random_sample()
    if i < args.level1:
        return 0
    elif i < (args.level1+args.level2):
        return 1
    elif i < (args.level1 + args.level2 + args.level3):
        return 2
    else:
        return 3

class KLLoss(nn.Module):
    def __init__(self, args):
        self.args = args
        super(KLLoss, self).__init__()

    def forward(self, pred, label):
        T=self.args.temperature
        
        predict = F.log_softmax(pred/T,dim=1)
        target_data = F.softmax(label/T,dim=1)
        target_data =target_data+10**(-7)
        with torch.no_grad():
            target = target_data.detach().clone()

        loss=T*T*((target*(target.log()-predict)).sum(1).sum()/target.size()[0])
        return loss


class Node:
    def __init__(self, nodeID, node_indices, args):
        self.id = nodeID
        self.node_indices = node_indices
        self.__args = args

    def train(self, device, lr, models, train_dataset):
        train_loader = DataLoader(Subset(train_dataset, self.node_indices), \
            batch_size=self.__args.batch_size, shuffle=True)

        optimizer_lst = []

        for model in models:
            model.train()
            model.to(device)
            optimizer = torch.optim.SGD(model.parameters(), lr=lr, weight_decay=1e-3)
            optimizer_lst.append(optimizer)

        criterion = nn.CrossEntropyLoss()
        criterion_kl = KLLoss(self.__args).cuda()
        
        for _ in range(self.__args.local_epoch):
            for batch_idx, (images, labels) in enumerate(train_loader):
                images, labels = images.to(device), labels.to(device)
                output_lst = []
                for i, model in enumerate(models):
                    optimizer = optimizer_lst[i]
                    optimizer.zero_grad()
                    output = model(images)
                    output_lst.append(output)

                for i, output in enumerate(output_lst):
                    optimizer = optimizer_lst[i]
                    loss = criterion(output_lst[i], labels)

                    if self.__args.kd == 1:
                        for j in range(len(models)):
                            if j == i:
                                continue
                            else:
                                loss += criterion_kl(output, output_lst[j].detach()) / (len(output_lst) - 1)
                            
                    loss.backward()
                    optimizer.step()

        models_params = []
        for model in models:
            model.to(torch.device('cpu'))
            models_params.append(copy.deepcopy(model.state_dict()))

        return models_params

def train_worker(device, trainq, resultq, args):

    processDone = False
    train_dataset, _ = get_dataset(args)

    while not processDone:

        msg = trainq.get()
        node = msg['node']

        if node == 'done':
            processDone = True
        
        else:
            lr = msg['lr']
            models = msg['models']
            model_weights = node.train(device, lr, models, train_dataset)
            resultq.put({'weights': copy.deepcopy(model_weights), 'model_num': copy.deepcopy(msg['model_num'])})
    
        del node
        del msg


def create_model(num_classes, ratio, track=True, scale=False):
    if args.dataset == 'cifar10':
        model = vggnet_sbn.make_VGG(num_classes, ratio, track, scale)

    elif args.dataset == 'cifar100':
        model = resnet_sbn.resnet18(num_classes, ratio, track, scale)

    elif args.dataset == 'tiny-imagenet':
        model = resnet_sbn_tiny.resnet34_tiny(num_classes, ratio, track, scale)

    return model


if __name__ == "__main__":

    mp.set_start_method('spawn')
    os.environ["OMP_NUM_THREADS"] = "1"

    if torch.cuda.is_available():
        n_devices = torch.cuda.device_count()
        devices = [torch.device("cuda:{}".format(i)) for i in range(n_devices)]
    else:
        n_devices = 1
        devices = [torch.device('cpu')]

    args = arguments.parser()

    if args.full == 1:
        args.level1 = 0.0
        args.level2 = 0.0
        args.level3 = 0.0
        args.level4 = 1.0

    num_nodes = args.nodes
    num_round = args.round
    num_local_epoch = args.local_epoch
    
    print("> Setting:", args)

    # load data
    nodeIDs = [i for i in range(num_nodes)]
    manager = mp.Manager()
    nodes = list()
    train_jobs = mp.Queue()
    resultQs = mp.Queue()
    preQ = queue.Queue()
    nodeindices = getNodeIndicies(nodeIDs, num_nodes, args)
    train_dataset, test_dataset = get_dataset(args)
    test_loader = DataLoader(test_dataset, 256, shuffle=False, num_workers=4)

    if args.dataset == "cifar10":
        num_classes = 10

    elif args.dataset == "cifar100":
        num_classes = 100
    
    elif args.dataset == "tiny-imagenet":
        num_classes = 200

    models = []
    param_sums = []
    base_count = []
    if args.base != -1:
        args.scale = 0
    
    for i in range(4):

        model = create_model(num_classes, ratio=1/2, track=True, scale=False)
        models.append(model)
        
        zero_param = {}
        for k,v in model.state_dict().items():
            zero_param[k] = torch.zeros(v.shape)

        param_sums.append(zero_param)
        base_count.append(0)
        

    for nodeID in nodeIDs:
        nodes.append((Node(nodeID, nodeindices[nodeID], args)))

    # round
    num_nodes_p = max(int(args.fraction * num_nodes), 1)
    train_processes = []

    for i in range(n_devices * args.n_procs):
        p = mp.Process(target=train_worker, args=(devices[i%n_devices], train_jobs, resultQs, args))
        p.start()
        train_processes.append(p)

    lr = args.lr
    np.random.seed(21)
    
    for r in range(1, num_round+1):
        if r % 5 == 0:
            print(f"Round {r}", end=', ')

        cur_time = time.time()
        lr *= args.lr_decay
    
        index_nodes_p = np.random.choice(range(num_nodes),
                                         num_nodes_p, replace=False)
        
        # train
        count = 0
        for num, i in enumerate(index_nodes_p):
            node = nodes[i]
            branch_num = select_level(node.id, args)
            model_num = np.random.choice(np.arange(4), branch_num + 1, replace=False)
            count += 1
            train_jobs.put({'node': copy.deepcopy(node), 'lr':lr, 'models': copy.deepcopy([models[i] for i in model_num]), 'model_num': copy.deepcopy(model_num)})

        
        for _ in range(count):
            result = resultQs.get()
            weights = result['weights']
            model_num = result['model_num']

            for i, model_idx in enumerate(model_num):
                for k,v in weights[i].items():
                    param_sums[model_idx][k] += v
                base_count[model_idx] += 1
            
        ## model avg
        
        with torch.no_grad():

            for i, model in enumerate(models):
                if base_count[i] > 0:
                    for k, v in param_sums[i].items():
                        v.div_(base_count[i])
                    
                    model.load_state_dict(param_sums[i])
                
            for i, param_sum in enumerate(param_sums):
                for k, v in param_sum.items():
                    param_sum[k] = torch.zeros(v.shape)
                
                base_count[i] = 0
                
        if r % 5 == 0:
            preQ.put({'round': r, 'models': copy.deepcopy(models)})
            print(f"Elapsed Time : {time.time()-cur_time:.1f}")

    for i in range(n_devices * args.n_procs):
        train_jobs.put({'node' : 'done'})

    for p in train_processes:
        p.join()

    # Train Finished
    time.sleep(5)
    # Test Start

    acc_list = {'multi': list(), 'single':list()}

    for _ in range(preQ.qsize()):
        msg = preQ.get()
        models = msg['models']

        for model in models:

            device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
            model = nn.DataParallel(model)
            model.to(device)
            model.eval()
            
        # Let's evaluate

        multi_correct, total = 0, 0
        accuracy_single_list = list()

        for i in range(len(models)):
            accuracy_single_list.append(0)

        with torch.no_grad():
            for batch_idx, (images, labels) in enumerate(test_loader):
                images, labels = images.to(device), labels.to(device)

                output_list = []
                for model in models:
                    output = model(images)
                    output_list.append(output)

                ensemble_output = torch.stack(output_list, dim=2)
                ensemble_output = torch.sum(ensemble_output, dim=2) / len(output_list)

                _, pred_labels = torch.max(ensemble_output, 1)
                pred_labels = pred_labels.view(-1)
                multi_correct += torch.sum(torch.eq(pred_labels, labels)).item()
                total += len(labels)

                for i, single in enumerate(output_list):  
                    _, pred_labels_single = torch.max(single, 1)
                    pred_labels_single = pred_labels_single.view(-1)
                    accuracy_single_list[i] += torch.sum(torch.eq(pred_labels_single, labels)).item()

            multi_acc= multi_correct/total
            for i in range(len(accuracy_single_list)):
                accuracy_single_list[i] /= total
    
        print("Round: {} / acc: {}".format(msg['round'], multi_acc))
        acc_list['multi'].append(multi_acc)
        acc_list['single'].append(accuracy_single_list)

    file_name = '../../save/{}/N[{}]_R[{}]_E[{}]_iid[{}]_F[{}]_LR[{}]_LD[{}]_B[{}]_RB[{}]_S[{}]_full[{}]_K[{}]_Ex[{}]_RC[{}]_split_mix.pkl'.\
		format(args.dataset, args.nodes, args.round, args.local_epoch, args.iid, \
            args.FedDyn, args.lr, args.lr_decay, args.base, args.randomBranch, args.scale, args.full, args.kd, args.extended, args.removeClient)

    with open(file_name, 'wb') as f:
        pickle.dump(acc_list, f)
