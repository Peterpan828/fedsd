import os
import torch
from torch.utils.data import DataLoader, Dataset
from torchvision import datasets, transforms
import torchtext
from torchtext.vocab import build_vocab_from_iterator
from torchtext.data.utils import get_tokenizer
from .sampling import noniid_nlp

class BatchDataset(Dataset):
    def __init__(self, dataset, seq_length) -> None:
        super().__init__()
        self.dataset = dataset
        self.seq_length = seq_length
        self.S = dataset[0].size(0)
        self.idx = list(range(0, self.S, seq_length))

    def __len__(self):
        return len(self.idx)
    
    def __getitem__(self, index):
        seq_length = min(self.seq_length, self.S - index)
        return self.dataset[:, self.idx[index]:self.idx[index]+seq_length]


def get_dataset(args):
    
    if args.dataset == 'mnist':
        apply_transform_train = transforms.Compose(
            [
             transforms.ToTensor(),
             transforms.Resize((32,32)),
             transforms.Normalize((0.1307),
                                  (0.3081)),
                                  ]
        )
        apply_transform_test = transforms.Compose(
            [transforms.ToTensor(),
            transforms.Resize((32,32)),
             transforms.Normalize((0.1307),
                                  (0.3081)),
                                  ]
        )
        dir = '~/fedsd/data/mnist'
        train_dataset = datasets.MNIST(dir, train=True, download=True,
                                         transform=apply_transform_train)
        test_dataset = datasets.MNIST(dir, train=False, download=True,
                                        transform=apply_transform_test)
        return train_dataset, test_dataset

    if args.dataset == 'cifar10':
        apply_transform_train = transforms.Compose(
            [transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
             transforms.ToTensor(),
             transforms.Normalize((0.4914, 0.4822, 0.4465),
                                  (0.2470, 0.2435, 0.2616)),
                                  ]
        )
        apply_transform_test = transforms.Compose(
            [transforms.ToTensor(),
             transforms.Normalize((0.4914, 0.4822, 0.4465),
                                  (0.2470, 0.2435, 0.2616)),
                                  ]
        )
        dir = '~/fedsd/data/cifar'
        train_dataset = datasets.CIFAR10(dir, train=True, download=True,
                                         transform=apply_transform_train)
        test_dataset = datasets.CIFAR10(dir, train=False, download=True,
                                        transform=apply_transform_test)
        return train_dataset, test_dataset

    if args.dataset == 'cifar100':
        apply_transform_train = transforms.Compose(
            [transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
             transforms.ToTensor(),
             transforms.Normalize((0.5071, 0.4867, 0.4408),
                                  (0.2675, 0.2565, 0.2761)),
                                  ]
        )
        apply_transform_test = transforms.Compose(
            [transforms.ToTensor(),
             transforms.Normalize((0.5071, 0.4867, 0.4408),
                                  (0.2675, 0.2565, 0.2761)),
                                  ]
        )
        dir = '~/fedsd/data/cifar100'
        train_dataset = datasets.CIFAR100(dir, train=True, download=True,
                                         transform=apply_transform_train)
        test_dataset = datasets.CIFAR100(dir, train=False, download=True,
                                        transform=apply_transform_test)

        return train_dataset, test_dataset

    if args.dataset == 'tiny-imagenet':
        apply_transform_train = transforms.Compose(
            [transforms.RandomRotation(20),
            transforms.RandomHorizontalFlip(0.5),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.4802, 0.4481, 0.3975],
                                std=[0.2302, 0.2265, 0.2262])
                                  ]
        )
        apply_transform_test = transforms.Compose(
            [transforms.ToTensor(),
            transforms.Normalize(mean=[0.4802, 0.4481, 0.3975],
                                std=[0.2302, 0.2265, 0.2262])
                                  ]
        )
        dir = '~/fedsd/data/tiny-imagenet/'
        train_dataset = datasets.ImageFolder(dir+'train', transform=apply_transform_train)
        test_dataset = datasets.ImageFolder(dir+'test', transform=apply_transform_test)

        return train_dataset, test_dataset

    if args.dataset == 'domainnet':
        apply_transform_train = transforms.Compose(
            [
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.7490, 0.7390, 0.7179],
                                std=[0.2188, 0.2179, 0.2245])
                                  ]
        )
        apply_transform_test = transforms.Compose(
            [
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.7490, 0.7390, 0.7179],
                                std=[0.2188, 0.2179, 0.2245])
                                  ]
        )
        dir = os.path.dirname(os.path.realpath(__file__))
        dir_split = dir.split('/')
        dir = '/'.join(dir_split[:-2]) 
        
        domains = ['clipart', 'infograph', 'painting', 'quickdraw', 'real', 'sketch']
        train_datasets = []
        test_datasets = []

        for domain in domains:
            cur_dir = dir + '/data/' + domain
            train_dataset = datasets.ImageFolder(cur_dir+'_train', transform = apply_transform_train)
            test_dataset = datasets.ImageFolder(cur_dir+'_test', transform = apply_transform_test)
            train_datasets.append(train_dataset)
            test_datasets.append(test_dataset)
        
        train_dataset = torch.utils.data.ConcatDataset(train_datasets)
        test_dataset = torch.utils.data.ConcatDataset(test_datasets)

        #print(len(train_dataset))
        #print(len(test_dataset))
        
        return train_dataset, test_dataset

    if args.dataset == 'wikitext-2':
        dir = "../../../root/fedsd/data/wikitext-2"
        train_iter = torchtext.datasets.WikiText2(dir, split='train')
        tokenizer = get_tokenizer('basic_english')
        vocab = build_vocab_from_iterator(map(tokenizer,train_iter), specials=['<unk>'])
        vocab.set_default_index(vocab['<unk>'])
        # print(len(vocab))

        train_iter, test_iter = torchtext.datasets.WikiText2(root=dir, split=('train', 'test'))
        def data_process(raw_text_iter):
            data = [torch.tensor(vocab(tokenizer(item)), dtype=torch.long) \
                for item in raw_text_iter]
            return torch.cat(tuple(filter(lambda t: t.numel() > 0, data)))

        def split(dataset: torch.Tensor):
            dataset = dataset[:(len(dataset) // args.nodes) * args.nodes]
            return dataset.reshape(args.nodes, -1)

        def batchify(dataset : torch.Tensor, batch_size):
            num_batch = len(dataset) // batch_size
            dataset = dataset.narrow(0, 0, num_batch * batch_size)
            dataset = dataset.reshape(batch_size, -1)
            return dataset

        train_dataset = data_process(train_iter)
        test_dataset = data_process(test_iter)
        train_datasets = split(train_dataset)
        train_datasets = [batchify(dataset, args.batch_size) for dataset in train_datasets]
        test_dataset = batchify(test_dataset, args.batch_size)
        
        return train_datasets, test_dataset

if __name__ == "__main__":
    
    import torch

    apply_transform = transforms.Compose(
        [transforms.ToTensor(),
        transforms.Resize((224,224))]
    )
    domains = ['clipart', 'infograph', 'painting', 'quickdraw', 'real', 'sketch']
    train_datasets = []
    for domain in domains:
        dir = '~/fedsd/data/' + domain + '_train'
        train_dataset = datasets.ImageFolder(dir, transform=apply_transform)
        train_datasets.append(train_dataset)
    
    dataset = torch.utils.data.ConcatDataset(train_datasets)
    TrainLoader = DataLoader(dataset, shuffle=False, num_workers=os.cpu_count(), batch_size=1)

    mean = torch.zeros(3)
    std = torch.zeros(3)

    for i, (inputs, labels) in enumerate(TrainLoader):
        if i % 10000 == 0:
            print(i)

        for j in range(3):
            mean[j] += inputs[:,j,:,:].mean()
            std[j] += inputs[:,j,:,:].std()
            
    mean.div_(len(dataset))
    std.div_(len(dataset))
    print(mean, std)


   
