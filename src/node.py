import torch
import torch.nn as nn
import torch.nn.functional as F
import copy
from torch.utils.data import DataLoader, Subset
from nn_models.resnet_sbn import resnet101
import numpy as np
from dataLoader.dataset import BatchDataset

# 0: no kd
# 1 : self distillation
# 2 : kd
# 3 : LS
# 4 : gradient rescaling
# 5 : similarity

def select_level(id, args):
    if args.randomBranch == 0:
        i = id / args.nodes
    else:
        i = np.random.random_sample()
    if i < args.level1:
        return 0
    elif i < (args.level1+args.level2):
        return 1
    elif i < (args.level1 + args.level2 + args.level3):
        return 2
    else:
        return 3


class KLLoss(nn.Module):
    def __init__(self, args):
        self.args = args
        super(KLLoss, self).__init__()

    def forward(self, pred, label):
        T=self.args.temperature
        
        predict = F.log_softmax(pred/T,dim=1)
        target_data = F.softmax(label/T,dim=1)
        target_data =target_data+10**(-7)
        with torch.no_grad():
            target = target_data.detach().clone()

        loss=T*T*((target*(target.log()-predict)).sum(1).sum()/target.size()[0])
        return loss

class KLLoss_topk(nn.Module):
    def __init__(self, args):
        self.args = args
        super(KLLoss_topk, self).__init__()

    def forward(self, pred, label, k):
        T=self.args.temperature
        
        predict = F.log_softmax(pred/T,dim=1)
        target_data = F.softmax(label/T,dim=1)
        values, indices = torch.topk(target_data, k)

        sum_topk = torch.sum(values, dim=1)
        equal_prob = (1 - sum_topk) / (target_data.shape[1] - k)
        equal_prob = equal_prob.reshape(equal_prob.shape[0],1)

        target_k = torch.ones_like(target_data)
        target_k = torch.mul(target_k, equal_prob)

        target_data = target_k.scatter(1, indices, values)
        # target_data =target_data+10**(-7)

        with torch.no_grad():
            target = target_data.detach().clone()

        loss=T*T*((target*(target.log()-predict)).sum(1).sum()/target.size()[0])
        return loss


class Loss_gradient_rescaling(nn.Module):
    def __init__(self, args):
        self.args = args
        super(Loss_gradient_rescaling, self).__init__()

    def forward(self, pred, teacher, label):
        T=self.args.temperature
        K = pred.size(1)

        predict = F.log_softmax(pred/T,dim=1)
        teacher_prob = F.softmax(teacher/T,dim=1)
        target_data = torch.ones_like(pred)

        for i in range(pred.shape[0]):
            true_index = label[i]
            confidence = teacher_prob[i][true_index]
            target_data[i] = target_data[i] * (1 - confidence) / (K-1)
            target_data[i][true_index] = confidence

        with torch.no_grad():
            target = target_data.detach().clone()

        loss=T*T*((target*(target.log()-predict)).sum(1).sum()/target.size()[0])
        return loss


class Loss_similarity(nn.Module):
    def __init__(self, args):
        self.args = args
        super(Loss_similarity, self).__init__()

    def forward(self, pred, weight, label):
        T=self.args.temperature
        K = pred.size(1)

        predict = F.log_softmax(pred/T,dim=1)
        target_data = torch.zeros_like(pred)

        for i in range(pred.shape[0]):
            true_index = label[i]
            weight_true = weight[true_index]
            target_data[i] = torch.matmul(weight_true, weight.T)

        target_data = F.relu(target_data)
        target_data = torch.pow(target_data, 0.3)
        target_data = F.softmax(target_data / 0.3, dim=1)

        with torch.no_grad():
            target = target_data.detach().clone()

        loss=T*T*((target*(target.log()-predict)).sum(1).sum()/target.size()[0])
        return loss

        
class Client:
    def __init__(self, nodeID, node_indices, prev_grads, args):
        self.nodeID = nodeID
        self.__node_indices = node_indices
        self.__args = args
        if args.FedDyn == 1:
            self.__prev_grads = prev_grads

        # if self.__args.test == 1 and select_level(self.nodeID, self.__args) == 3:
        #     self.teacher = self.create_model()
        #     self.teacher.load_state_dict(torch.load("../save/Models/CIFAR100.pt"))

        #     self.logit_weight = self.teacher.state_dict()['fc.weight'].detach().clone()
        #     self.logit_weight = torch.nn.functional.normalize(self.logit_weight, p=2.0)
        
        self.mask_rate = 0.15
        self.num_tokens = 33278
        
    def create_model(self):
        if self.__args.dataset == 'cifar100':
            model = resnet101(num_class=100)
            return model

    def get_prev_grads(self):
        return copy.deepcopy(self.__prev_grads)

    def set_prev_grads(self, prev_grads):
        self.__prev_grads = prev_grads
    
    def train(self, device, lr, consistency_weight, model, train_dataset):
        
        if self.__args.dataset != 'wikitext-2':
            train_loader = DataLoader(Subset(train_dataset, self.__node_indices), \
                batch_size=self.__args.batch_size, shuffle=True)
            
            model.train()
            model.to(device)

            if self.__args.FedDyn == 1:
                with torch.no_grad():
                    server_params = {k: param.flatten().clone() for (k, param) in model.named_parameters()}

                for k, param in model.named_parameters():
                    self.__prev_grads[k] = self.__prev_grads[k].to(device)
            
            optimizer = torch.optim.SGD(model.parameters(), lr=lr, weight_decay=1e-3)

            if self.__args.kd == 3:
                criterion = nn.CrossEntropyLoss(label_smoothing=0.1)
            else:
                criterion = nn.CrossEntropyLoss()

            criterion_kl = KLLoss(self.__args).cuda()
            criterion_gradient_rescaling = Loss_gradient_rescaling(self.__args).cuda()
            criterion_similarity = Loss_similarity(self.__args).cuda()
            
            if self.__args.kd == 0 or self.__args.kd == 1 or self.__args.kd == 3: # w/o KD, SD, LS
                coefficient = 1
                
            else:
                coefficient = 0.7

            
            for _ in range(self.__args.local_epoch):
                for _, (images, labels) in enumerate(train_loader):
                    images, labels = images.to(device), labels.to(device)
                    loss = 0.
                    optimizer.zero_grad()
                    output_list, feature_list = model(images)

                    if self.__args.kd == 2 or self.__args.kd == 4:
                        with torch.no_grad():
                            teacher_output = self.teacher(images)

                    for i, (branch_output, branch_feature) in enumerate(zip(output_list, feature_list)):
                        
                        if self.__args.test == 1 and i != 3:
                            continue
                        
                        if self.__args.extended == 0 and i != len(output_list) - 1:
                            continue

                        loss += coefficient * criterion(branch_output, labels) ## true label, branch output loss
                        
                        if self.__args.kd == 1:
                            if len(output_list) > 1:
                                for j in range(len(output_list)):
                                    if j == i:
                                        continue
                                    else:
                                        loss += consistency_weight * criterion_kl(branch_output, output_list[j].detach()) / (len(output_list) - 1) ## Self distillation term

                        if self.__args.kd == 2:
                            loss += 0.3 * criterion_kl(branch_output, teacher_output.detach()) ## Self distillation term

                        elif self.__args.kd == 4:
                            loss += 0.3 * criterion_gradient_rescaling(branch_output, teacher_output.detach(), labels.detach())

                        elif self.__args.kd == 5:
                            loss += 0.3 * criterion_similarity(branch_output, self.logit_weight, labels.detach())

                    if self.__args.FedDyn == 1:
                        for k, param in model.named_parameters():
                            curr_param = param.flatten()
                            ## linear penalty

                            lin_penalty = torch.dot(curr_param, self.__prev_grads[k])
                            loss -= lin_penalty

                            ## quadratic penalty
                            
                            quad_penalty = self.__args.alpha/2.0 * torch.sum(torch.square(curr_param - server_params[k]))
                            loss += quad_penalty

                    loss.backward()
                    optimizer.step()

            if self.__args.FedDyn == 1:
                # update prev_grads
                with torch.no_grad():
                    for k, param in model.named_parameters():
                        curr_param = param.flatten().clone()
                        self.__prev_grads[k] = self.__prev_grads[k] - self.__args.alpha * (curr_param - server_params[k])
                        self.__prev_grads[k] = self.__prev_grads[k].to(torch.device('cpu'))

            model.to(torch.device('cpu'))

            # if select_level(self.nodeID, self.__args) == 3:
            #     self.teacher.to(torch.device('cpu'))

            weight = model.state_dict()

            return copy.deepcopy(weight)
        
        else:
            
            train_loader = BatchDataset(train_dataset[self.nodeID], self.__args.sequence_length)
            # print(len(train_loader))
            
            model.train()
            model.to(device)

            if self.__args.FedDyn == 1:
                with torch.no_grad():
                    server_params = {k: param.flatten().clone() for (k, param) in model.named_parameters()}

                for k, param in model.named_parameters():
                    self.__prev_grads[k] = self.__prev_grads[k].to(device)
            
            optimizer = torch.optim.SGD(model.parameters(), lr=lr, weight_decay=1e-3)
            criterion = nn.CrossEntropyLoss()
            criterion_kl_topk = KLLoss_topk(self.__args).cuda()
            criterion_kl = KLLoss(self.__args).cuda()

            for _ in range(self.__args.local_epoch):
                for _, input in enumerate(train_loader):
                    input = input.to(device)

                    mask = torch.rand(input.shape) < 0.15
                    mask_change = mask & (torch.rand(input.shape) < 0.9)
                    mask_random = mask_change & (torch.rand(input.shape) < 1/9)

                    output_list = model(input, mask_change, mask_random)   
                    loss = 0.
                    optimizer.zero_grad()       
                    
                    for i, branch_output in enumerate(output_list):

                        # if i != 3:
                        #     continue

                        loss += criterion(branch_output[mask], input.detach()[mask]) ## true label, branch output loss
            
                        if len(output_list) > 1:
                            if self.__args.kd == 1:
                                for j in range(len(output_list)):
                                    if j == i:
                                        continue
                                    else:
                                        # loss += consistency_weight * criterion_kl_topk(branch_output[mask], output_list[j].detach()[mask], 100) / (len(output_list) - 1) ## Self distillation term
                                        loss += consistency_weight * criterion_kl(branch_output[mask], output_list[j].detach()[mask]) / (len(output_list) - 1) ## Self distillation term

                    if self.__args.FedDyn == 1:
                        for k, param in model.named_parameters():
                            curr_param = param.flatten()
                            ## linear penalty

                            lin_penalty = torch.dot(curr_param, self.__prev_grads[k])
                            loss -= lin_penalty

                            ## quadratic penalty
                            
                            quad_penalty = self.__args.alpha/2.0 * torch.sum(torch.square(curr_param - server_params[k]))
                            loss += quad_penalty

                    loss.backward()
                    optimizer.step()

            if self.__args.FedDyn == 1:
                # update prev_grads
                with torch.no_grad():
                    for k, param in model.named_parameters():
                        curr_param = param.flatten().clone()
                        self.__prev_grads[k] = self.__prev_grads[k] - self.__args.alpha * (curr_param - server_params[k])
                        self.__prev_grads[k] = self.__prev_grads[k].to(torch.device('cpu'))

            model.to(torch.device('cpu'))
            weight = model.state_dict()

            return copy.deepcopy(weight)
