#!/bin/bash

python main.py --dataset='cifar100' --randomBranch=1;
python main.py --dataset='cifar100' --randomBranch=1 --removeBranch=3;
python main.py --dataset='cifar100' --randomBranch=1 --removeBranch=2;
python main.py --dataset='cifar100' --randomBranch=1 --removeBranch=1;