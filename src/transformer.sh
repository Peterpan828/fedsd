#!/bin/bash

python main.py --dataset='wikitext-2' --round=150 --local_epoch=1 --batch_size=64 --lr_decay=0.98 --consistency_rampup=120 --n_procs=1 --FedDyn=0;
python main.py --dataset='wikitext-2' --round=150 --local_epoch=1 --batch_size=64 --lr_decay=0.98 --consistency_rampup=120 --kd=0 --n_procs=1 --FedDyn=0;
python main.py --dataset='wikitext-2' --round=150 --local_epoch=1 --batch_size=64 --lr_decay=0.98 --consistency_rampup=120 --n_procs=1 --randomBranch=1 --FedDyn=0;
python main.py --dataset='wikitext-2' --round=150 --local_epoch=1 --batch_size=64 --lr_decay=0.98 --consistency_rampup=120 --n_procs=1 --randomBranch=1 --kd=0 --FedDyn=0;
python main.py --dataset='wikitext-2' --round=150 --local_epoch=1 --batch_size=64 --lr_decay=0.98 --consistency_rampup=120 --n_procs=1 --full=1 --FedDyn=0;
python main.py --dataset='wikitext-2' --round=150 --local_epoch=1 --batch_size=64 --lr_decay=0.98 --consistency_rampup=120 --n_procs=1 --full=1 --kd=0 --FedDyn=0;

# python main.py --dataset='wikitext-2' --round=150 --local_epoch=1 --batch_size=64 --lr_decay=0.98 --consistency_rampup=120 --iid=0 --n_procs=1 --beta=2.0;
# python main.py --dataset='wikitext-2' --round=150 --local_epoch=1 --batch_size=64 --lr_decay=0.98 --consistency_rampup=120 --iid=0 --kd=0 --n_procs=1 --beta=2.0;