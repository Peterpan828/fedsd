import copy
import pickle
import os
import torch
from models import evaluate, evaluate_simple
from dataLoader.dataset import get_dataset, BatchDataset
from nn_models.resnet_sbn import resnet101
from torch.utils.data import DataLoader
import numpy as np
import random

def create_model(args):
	if args.dataset == 'cifar100':
		model = resnet101(num_class=100)
		return model

def mv_average_10(lst):
    mv_lst = []
    for i in range(len(lst)-10):
        mv_lst.append(round(sum(lst[i:i+10]) / 10, 4))
    return mv_lst


def mv_average_20(lst):
    mv_lst = []
    for i in range(len(lst)-20):
        mv_lst.append(round(sum(lst[i:i+20]) / 20, 4))
    return mv_lst


def gpu_train_worker(trainID, trainQ, resultQ, device, args):

	torch.manual_seed(2)
	np.random.seed(2)
	random.seed(2)

	train_dataset, _ = get_dataset(args)

	while True:
		msg = trainQ.get()

		if msg == 'kill':
			break

		elif msg['type'] == 'train':
			
			processing_node = msg['node']
			model = msg['model']
			model_weight = processing_node.train(device, msg['lr'], msg['consistency_weight'], model, train_dataset)
			result = {'weight':copy.deepcopy(model_weight), 'id':processing_node.nodeID}
		
			if args.FedDyn == 1:
				result['prev_grads'] = copy.deepcopy(processing_node.get_prev_grads())
			else:
				result['prev_grads'] = None
			resultQ.put(result)
		del processing_node
		del model
		del model_weight
		del msg

	#print("train end")

def gpu_test_worker(testQ, device, args):

	torch.manual_seed(3)
	np.random.seed(3)
	random.seed(3)

	global_acc = {'multi':list(), 'single':list()}
	_, test_dataset = get_dataset(args)

	if args.dataset == 'wikitext-2':
		test_loader = BatchDataset(test_dataset, args.sequence_length)
	else:
		test_loader = DataLoader(test_dataset, 256, shuffle=False, num_workers=4)

	while True:
		msg = testQ.get()

		if msg == 'kill':
			break

		else:
			if args.interpolation == 1:
				levels = msg['levels']

			model = msg['model']
			round = msg['round']
			
			acc_multi, acc_single, loss = evaluate(model, test_loader, args, device)
			global_acc['multi'].append(acc_multi)
			global_acc['single'].append(acc_single)
		
			print("Round: {} / Multi Accuracy (Perplexity): {:.4f}".format(round, acc_multi))
			print("Round: {} / Single Accuracy (Perplexity): {:.4f}".format(round, acc_single[0]))
	
	# if args.full == 0:
	# 	torch.save(model.state_dict(), '../save/Models/{}_K[{}]_student.pt'.format(args.dataset, args.kd))

	# else:
	# 	torch.save(model.state_dict(), '../save/Models/{}_K[{}]_student_full.pt'.format(args.dataset, args.kd))

	if args.kd == 2:
		
		teacher = create_model(args)  
		teacher.load_state_dict(torch.load("../save/Models/CIFAR100.pt"))
		teacher.to(device)
		acc_multi, acc_single, loss = evaluate_simple(teacher, test_loader, args, device)

		print("Teacher: {}".format(acc_multi))


	if args.interpolation == 1:
		file_name = '../save/{}/iid[{}]_K[{}].pkl'.format(args.dataset, args.iid, args.kd)

		if args.iid == 1:
			mv_acc = mv_average_10(global_acc['multi'])
		else:
			mv_acc = mv_average_20(global_acc['multi'])

		if(os.path.exists(file_name)):
			with open(file_name, 'rb') as f:
				interpolation_dict = pickle.load(f)
			
			interpolation_dict['levels'].append(levels)
			interpolation_dict['accuracy'].append(mv_acc[-1])

		else:
			interpolation_dict = {}
			interpolation_dict['levels'] = [levels]
			interpolation_dict['accuracy'] = [mv_acc[-1]]

		with open(file_name, 'wb') as f:
			pickle.dump(interpolation_dict, f)

	else:
		if args.base == -1:
			file_name = '../save/{}/N[{}]_iid[{}]_K[{}]_F[{}]_P[{},{},{},{}]_decay[{}]_R[{}]_RB[{}]_FR[{}]_T[{}]_RM[{}]_RC[{}]_Ex[{}]_main.pkl'.\
				format(args.dataset, args.nodes, \
					args.iid, args.kd, args.FedDyn,\
						args.level1, args.level2, args.level3, args.level4, args.lr_decay, args.consistency_rampup, args.randomBranch, args.fraction, args.test, args.removeBranch, args.removeClient, args.extended)
		else:
			file_name = '../save/{}/N[{}]_R[{}]_iid[{}]_K[{}]_F[{}]_B[{}]_base.pkl'.\
				format(args.dataset, args.nodes, args.round, \
					args.iid, args.kd, \
						args.FedDyn, args.base)

		with open(file_name, 'wb') as f:
			pickle.dump([global_acc], f)
		
		#print("test end")
